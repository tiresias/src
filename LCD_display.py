import RPi.GPIO as GPIO 
import time
from RPLCD import CharLCD

 
lcd = CharLCD(numbering_mode=GPIO.BCM, cols=16, rows=2, pin_rs=25, pin_e=22, pins_data=[17, 21, 27, 12])
			 
	if distance >5:
		lcd.write_string ("Safe to Switch")
		
	else:
		lcd.write_string ("Warning Vehicle ")
		lcd.write_string (distance)
		lcd.write_string (" cm")
		sleep (1)