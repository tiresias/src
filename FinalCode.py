import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
import time
from RPLCD import CharLCD

TRIG = 23
ECHO = 24
BUZZER = 18

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

lcd = CharLCD(numbering_mode=GPIO.BCM, cols=16, rows=2, pin_rs=25, pin_e=22, pins_data=[17, 21, 27, 12])
             
while True:
    print("distance measurement in progress")
    GPIO.setup(TRIG,GPIO.OUT)
    GPIO.setup(ECHO,GPIO.IN)
    GPIO.setup(BUZZER,GPIO.OUT)
    GPIO.output(TRIG,False)
    print ("waiting for sensor to settle")
    time.sleep(2)
    GPIO.output(TRIG,True)
    time.sleep(0.00001)    #wait 10 micro second
    GPIO.output(TRIG, False)
    
    while GPIO.input(ECHO)==0:    #wait till ECHO is LOW
        pulse_start=time.time()
        
    while GPIO.input(ECHO)==1:    #wait here till ECHO is HIGH
        pulse_end=time.time()
        
    pulse_duration=pulse_end-pulse_start
    distance=pulse_duration*17150
    distance=round(distance,2)
    print("distance:",distance," cm")
    
    if distance >5:
        GPIO.output(BUZZER, GPIO.LOW)
        print("Buzzer off")
        lcd.write_string ("Safe to Switch")
    else:
        print("Buzzer on")
        GPIO.output(BUZZER, GPIO.HIGH)
        lcd.write_string ("Warning Vehicle ")
		lcd.write_string (distance)
		lcd.write_string (" cm")
		sleep (1)
        
GPIO.cleanup()